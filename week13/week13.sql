select count(*) from proteins;

explain select*from proteins where pid like "5HT2C_HUMA%";

create index idx1 on proteins(pid);
create unique index idx2 on proteins(accession);
create unique index idx3 on proteins(accession, pid);
alter table proteins add constraint acc_pk primary key (accession);

alter  table proteins drop index idx3;