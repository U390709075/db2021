select*from denormalized;

load data 
infile "C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\denormalized_movie_db.csv"
#infile "C:\\Users\\bardi\\Desktop\\denormalized_movie_db.csv"
into table denormalized
columns terminated by ';';

show variables like "secure_file_priv";

select movie_id, title, ranking, year, votes, duration, oscars, budget
from denormalized;

insert into movies (movie_id, title, ranking, rating, year, votes, duration, oscars, budget)
select distinct movie_id, title, ranking, rating, year, votes, duration, oscars, budget
from denormalized;

select*from movies;

delete from movies;