use company;

UPDATE customers SET Country = REPLACE(Country,'\n', "");

select count(CustomerID), Country
from customers
group by Country
order by count(CustomerID) desc;

create view usa_customers as
select CustomerName,ContactName
from customers
where Country ="USA";

select*from usa_customers;

create or replace view products_above_avg as
select ProductName,Price
from products
where Price > (select avg(Price)from products);

select*from products_above_avg 





